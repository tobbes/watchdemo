//
//  MyRow.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 22/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import WatchKit

class MyRow: NSObject {
    
    override init() {
        self.reorderBlock = {}
        super.init()

    }
    @IBOutlet var topGroup: WKInterfaceGroup!
    @IBOutlet var productTitle: WKInterfaceLabel!
    @IBOutlet var buyButton: WKInterfaceButton!
    @IBOutlet var backgroundGroup: WKInterfaceGroup!

    @IBOutlet var titleGroup: WKInterfaceGroup!
    @IBOutlet var buybuttonGroup: WKInterfaceGroup!
    @IBOutlet var buttonImage: WKInterfaceImage!
    var reorderBlock:() ->Void
    
    @IBAction func doOrder() {
    reorderBlock()
    }
    
    

}

