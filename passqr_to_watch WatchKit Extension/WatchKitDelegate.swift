//
//  WatchKitDelegate.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 14/12/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import WatchKit
import WatchConnectivity
class WatchKitDelegate: NSObject, WCSessionDelegate {

    override init() {
        super.init()
        let session = WCSession.defaultSession()
        session.delegate = self
        session.activateSession()
    
    }
    func session(session: WCSession, var didReceiveUserInfo userinfo: [String : AnyObject]) {
        print("DID RECEIEVE USER DATA");
        let backend = WatchBackend.getInstance()
        if(userinfo.count == 0) {
            backend.removeAll()
            return
        }
        
        
        let order = ReorderItem(dict:userinfo)
        
        var exists = false;
        var i = 0;
        
        while(!exists && i < backend.latestOrders.count) {
            
            if(order.productId == backend.latestOrders[i].productId) {
                exists = true;
            }
            i++
            
        }
        
        if(exists) {
            return
        }
        
        backend.latestOrders.insert(order, atIndex: 0)
        backend.addNewImage(order.imagePath)
        
        
        if(userinfo["MAX"] != nil) {
            let max:Int = userinfo["MAX"] as! Int
            let toRemove = backend.latestOrders.count - max
            if(toRemove > 0) {
                backend.remove(toRemove)
            }
        }
        
        
        let done_not = NSNotification(name: "RECEIVED_ORDERS", object: self)
        
        let center = NSNotificationCenter.defaultCenter()
        center.postNotification(done_not)
        
        
        
    }
    
    func session(session: WCSession, didReceiveFile file: WCSessionFile) {
        
        let backend = WatchBackend.getInstance()
        if let filepath = backend.processFile(file.fileURL) {
            
            if(file.metadata != nil) {
                backend.processQr(file.metadata!, filepath: filepath)
            }
        }
        
        backend.notify_all()
        
                
        
        
    }


}
