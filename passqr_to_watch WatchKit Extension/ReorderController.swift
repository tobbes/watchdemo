    //
//  ReorderController.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 15/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class ReorderController: WKInterfaceController {
    @IBOutlet var productTable: WKInterfaceTable!
    //var backend:WatchBackend
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        let centre = NSNotificationCenter.defaultCenter()
        centre.addObserver(self, selector: Selector("receiveNot:"), name: "RECEIVED_ORDERS", object: nil)
        centre.addObserver(self, selector: "receiveNot:", name: "ORDERS_EMPTY", object: nil)
        centre.addObserver(self, selector: "receiveNot:", name: "SET_ORDERS_X", object: nil)
        
        
        
    }
    
    
    private func reinit() ->Void {
    
      let backend = WatchBackend.getInstance()
       let cache = Cache()
        
       self.productTable.setNumberOfRows(backend.latestOrders.count, withRowType: "myRow")
    
        for(var i = 0 ; i < backend.latestOrders.count ; i++){
                   
            let row = self.productTable.rowControllerAtIndex(i) as! MyRow
        
            row.backgroundGroup.setBackgroundImage(nil)
            row.productTitle.setText("");
            let order = backend.getOrder(i)
            
            let setter = {(image:UIImage) -> Void in
            
                let todo =  {() in
                    
                    if(self.productTable != nil) {
                    
                    let index = backend.getIndexForImage(order.imagePath)
                    let myrow = self.productTable.rowControllerAtIndex(index) as! MyRow
                    myrow.backgroundGroup.setBackgroundImage(image)
                    }
                    
                    }
                
                   // dispatch_async(dispatch_async(<#T##queue: dispatch_queue_t##dispatch_queue_t#>, <#T##block: dispatch_block_t##dispatch_block_t##() -> Void#>), <#T##block: dispatch_block_t##dispatch_block_t##() -> Void#>)
                    dispatch_async(dispatch_get_main_queue(), todo)
                
            
            }
            //let url = NSURL(fileURLWithPath: order.imagePath)
            
            //let url = NSURL(string: order.imagePath)
            
            cache.getImage(order.imagePath, todo: setter,index: i)
            
            
                
            
            
            let title = "\(order.productName) \(order.Price) :-"
            row.productTitle.setText(title)
            
            
            
            
            //self.downloadImage(order.imagePath as String)
            
            
            row.reorderBlock = self.makeBlock(i)
            
        }
    
        return
    
    }
    

    private func  makeBlock(id:Int) -> () ->Void {
    
        let f = { () -> Void in
            self.placeOrder(id)
        }
        
        return f
    }
    
    
    
    
       override func willActivate() {
        let centre = NSNotificationCenter.defaultCenter()
        centre.addObserver(self, selector: Selector("receiveNot:"), name: "RECEIVED_ORDERS", object: nil)
        centre.addObserver(self, selector: "receiveNot:", name: "ORDERS_EMPTY", object: nil)
        centre.addObserver(self, selector: "receiveNot:", name: "SET_ORDERS_X", object: nil)
        let backend = WatchBackend.getInstance()
        
        if(!backend.isPaired()) {
            
            let action = WKAlertAction(title: "OK", style: .Default, handler: {})
            
        }
        
        
        
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        self.reinit()
        
        //let backend = WatchBackend.getInstance()
       
        
        //self.reinit()
        
            }
    
    
    //This function is called when the controller receives a notification
    func receiveNot(not:NSNotification) {
        switch(not.name) {
        case "ORDERS_EMPTY": print("Received ORDERS_EMPTY"); self.reinit(); break;
        case "SET_ORDERS_X": print("Received SET_ORDERS_X"); self.reinit(); break;
        case "RECEIVED_ORDERS": self.reinit(); break;
        default: print("UNKNOWN NOTIFICATION!"); break
        }
        self.reinit()
    }
    
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
        super.didDeactivate()
    }
    
    // callback which reorders an item
    private func placeOrder(id:Int) {
       
        let backend = WatchBackend.getInstance()
        let order = backend.getOrder(id)
       
        let success = {
            let action = WKAlertAction(title: "OK", style: WKAlertActionStyle.Default, handler: {})
            self.presentAlertControllerWithTitle("Tack för din beställning", message: "Du beställde \(order.productName)" , preferredStyle: WKAlertControllerStyle.Alert, actions: [action])
        }

        
        
        
        
        let errorReporter = {
            (title:String,message:String) -> Void in
        
            let action = WKAlertAction(title: "OK", style: .Default, handler: {})
            self.presentAlertControllerWithTitle(title, message: message, preferredStyle: .Alert, actions: [action])
        }
        
        
        let no = WKAlertAction(title: "NEJ", style: .Default, handler: {})
        let yes = WKAlertAction(title: "JA", style: WKAlertActionStyle.Default,
            handler: {backend.sendOrder(order, numtries: 3, errorReporter: errorReporter,success: success) }
)
                self.presentAlertControllerWithTitle("Genomföra köp?", message: "Köpa denna produkt?", preferredStyle: .Alert, actions: [yes,no])
        
    
    }
    
    
    
    
        
    }
    
