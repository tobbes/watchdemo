        //
//  WatchBackend.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 09/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
// This class provides the logic of the Watchapp

import UIKit
import Foundation
import WatchConnectivity
class WatchBackend: NSObject {//,WCSessionDelegate {
    private static let sharedInstance = WatchBackend()
    
    private var imageIndeces:[String:Int] = [:]
    var latestOrders:[ReorderProtocol] = []
    var maxOrders:Int = -1
    var session:WCSession?
    let DEBUG = true
    override init() {
        self.session = nil
        super.init()
        if(WCSession.isSupported()) {
            self.session = WCSession.defaultSession()
            session!.activateSession()
        }
            
        else {
            print("WatchConnectivity not supported!")
            self.session = nil
        }
      
}
    
    /* Gets the index of the row to display the image with the given url at
        in the table in ReoderController
    */
    func getIndexForImage(ImageName:String) -> Int {
        return self.imageIndeces[ImageName]!
    }
    
    
    
    /* Checks if the watch currently is paired with a phone */
    func isPaired() ->Bool {
        
        if(self.session == nil) {
            return false;
        }
            return self.session!.reachable
        
    }
    
    
    
    func getOrder(index:Int) -> ReorderProtocol {
        return self.latestOrders[index]
    }
    
    
    
    
    
    
    static func getInstance() ->WatchBackend {
        let instance = self.sharedInstance ?? WatchBackend()
        return instance
    }
    
    /* This function calls the phone to reorder the provided item */
    // numtries is the number of times to try before giving up
    //success is run if the reorder was successful
    //errorreporter is run if the reorder for some reason fails
    func sendOrder(order:ReorderProtocol,numtries:Int,errorReporter:(title:String,message:String) ->Void,success:() ->Void) -> Void {
        
        //We failed to send the request to the phone
        if(numtries < 1) {
            errorReporter(title:"Could not send your order",message: "DEVICE NOT REACHABLE")
            return
        }
        
        if(self.session == nil ) {
            return;
        }
        
        // convert reorderitem to a dictionary
        let dictrep = order.toDict() as AnyObject
        let message = ["ORDER": dictrep]
        
        
        
        
        //Create the block to be run if the reorder succeeds
        let replyHandler = {(reply:[String:AnyObject]) ->Void in
            
            let rep = reply["REPLY"] as! String
            print("Did receive reply: \(rep)");
            //success()
                    
            if(rep == "SUCESS") {
                
            success()
            
            }
            
            else {
                errorReporter(title: "Misslyckades med beställningen",message: "Kunde inte lägga beställningen")
            }
            
        }
        
        let errorhandler = { (error:NSError) -> Void in
            errorReporter(title: "INTERNAL ERROR",message: error.localizedDescription)
        }
        
        
        
        
       if(self.session!.reachable) {
                self.session?.sendMessage(message, replyHandler: replyHandler, errorHandler: errorhandler)
                return;
        }
        else {
        //if the handheld is not in a reachable state then delay 5 seconds and try again
        let newnumtries = numtries-1
        let recall = {() -> Void in
            
            
            self.sendOrder(order, numtries: newnumtries,errorReporter: errorReporter,success: success)
        }
        
        let delayInSeconds = 5 as UInt64
        let popTime = dispatch_time(DISPATCH_TIME_NOW,Int64(delayInSeconds * NSEC_PER_SEC))
        
        dispatch_after(popTime, dispatch_get_main_queue(), recall)
        
        
        }
        
            
        
        }
    
    
    // Update the indices for the images in the list in reorderController
    func updateImageIndex(amount:Int) {
    
    let keys = self.imageIndeces.keys
    for key in keys {
    let val = imageIndeces[key]! - amount
    imageIndeces[key] = val
    }
    }

    
    func addNewImage(imageName:String){
        
        let indexForNew = 0;
        self.updateImageIndex(-1)
        imageIndeces[imageName] = indexForNew
        
    }
    
    func addNewImage(ImageName:String, Index:Int) {
        imageIndeces[ImageName] = Index
    }
    
    
    
    
    
    
    
    
    func remove(howMany:Int) {
        
        
        if(howMany > self.latestOrders.count) {
            self.latestOrders.removeAll()
            self.imageIndeces.removeAll()
        }
        
        else {
            for var i = 0 ; i < howMany ; i++
            {
                let order = self.latestOrders.removeLast()
                self.imageIndeces.removeValueForKey(order.imagePath)
            }
        }
        
        
        
        //self.latestOrders.removeLast()
        self.notify_removed_SpecificNumber()
    
    }
    
    
    
    func notify_emptyOrders() ->Void {
        let notif = NSNotification(name: "ORDERS_EMPTY", object: self);
        NSNotificationCenter.defaultCenter().postNotification(notif);
        
    }
    
    func notify_removed_SpecificNumber() {
        
        let notif = NSNotification(name: "SET_ORDERS_X", object: self);
        NSNotificationCenter.defaultCenter().postNotification(notif)
        
    }
    
    
    func notify_all() {
        // let notif = NSNotification(name: "QRIMAGE_UPDATED",object: self)
        let notif = NSNotification(name: "QRIMAGE_UPDATED", object: self, userInfo:nil)
        
        //notify the qrcode view of the change
        NSNotificationCenter.defaultCenter().postNotification(notif)
    }
    

    
    
    
     func processQr(metadata:[String:AnyObject],filepath:String) ->Void {
        var title:NSString?
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(filepath, forKey: "IMAGE_PATH")
        defaults.synchronize()
        print("THIS IS THE FILEPATH: \(filepath)")
        
        //if let metainfo = file.metadata {
        title = metadata["TITLE"] as!String?
        let subtitle = metadata["SUBTITLE"]
        var subTitleText:String
        if(subtitle != nil){
            subTitleText = subtitle! as! String
        }
        else {
            subTitleText = "Scan your qrcode"
        }
        defaults.setObject(subTitleText, forKey: "SUBTITLE")
        defaults.synchronize()
        
        
        if(title != nil) {
            defaults.setObject(title, forKey: "TITLE")
            defaults.synchronize()
            //self.showImage(filepath, title: title! as String,subtitle:subTitleText)
        }
            
        else{
            //self.showImage(filepath, title: "",subtitle:subTitleText)
            
            
        }//}
        
        
        
        
        
        
    }
    

    
    
    
    
    
    
    func processFile(file:NSURL) ->String? {
        
        let destpath = getDestPath()
        let filemanager = NSFileManager.defaultManager()
        
        if(filemanager.fileExistsAtPath(destpath)) {
            do {
                try filemanager.removeItemAtPath(destpath)
            }
            catch {
                print("Could not remove file at path: \(destpath)")
                return nil
            }
            
        }
        
        do{
            try filemanager.moveItemAtURL(file, toURL: NSURL.init(fileURLWithPath: destpath))
        }
        catch {
            print("Could not move file: \(file.absoluteString) to \(destpath)")
            return nil
        }
        
        return destpath
        
    }
    
    
    private func getDestPath() -> String {
       let path = NSTemporaryDirectory() + "image.png"
       return path
        
}


    func persist() ->Void {
        
        var dicts:[[String:AnyObject]] = [];
        
        for r in self.latestOrders {
            
            dicts.append(r.toDict())
        }
    
        let userdefaults = NSUserDefaults.standardUserDefaults()
        userdefaults.setObject(dicts, forKey: "SAVED_ORDERS")
        userdefaults.synchronize()
        
        
    

}

    
    func removeAll() {
        self.latestOrders.removeAll()
        self.imageIndeces = [:]
        notify_emptyOrders();
        
    }
    

}
  