//  ExtensionDelegate.swift
//  passqr_to_watch WatchKit Extension
//
//  Created by Tobias Ednersson on 07/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import WatchKit
import WatchConnectivity

class ExtensionDelegate: NSObject, WKExtensionDelegate {//,WCSessionDelegate {
    let DEBUG = true
    var delegate:WatchKitDelegate = WatchKitDelegate()
    //var session:WCSession
    let backend = WatchBackend.getInstance()
    //var session:WCSession = WCSession.defaultSession()
    func applicationDidFinishLaunching() {
       
        let defaults = NSUserDefaults.standardUserDefaults();
        delegate = WatchKitDelegate()
        let backend = WatchBackend.getInstance()

        if( defaults.objectForKey("SAVED_ORDERS")  != nil){
            let orders = defaults.objectForKey("SAVED_ORDERS") as! [[String:AnyObject]]
            var index = 0;
            for dict in orders  {
            let reorder = ReorderItem(dict: dict)
            backend.latestOrders.append(reorder)
            backend.addNewImage(reorder.imagePath,Index: index)
            index++
            }
            
            backend.notify_all()
            
        
        }
        
        if(WCSession.isSupported()) {
        //let session = WCSession.defaultSession()
        //session.delegate = self;
        //session.activateSession()
        }
        
        else {
            print("WatchConnectivity not supported!")
        }
        
        // Perform any final initialization of your application.
    }

    
    func applicationDidBecomeActive() {
        //let backend = WatchBackend.getInstance()
        //backend.notify_all()
        
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
       
        let backend = WatchBackend.getInstance()
        backend.persist()
        
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }
    


    
    /*func sendOrder() -> Void {
        
        
        let message = ["ORDER":55];
        
        let replyHandler = {(reply:[String:AnyObject]) ->Void in
            
            let rep = reply["REPLY"] as! String
            
            print("Did receive reply: \(rep)");
            
        }
        
        let errorhandler = { (error:NSError) -> Void in
        print("Error: \(error.localizedDescription)")
            
        }
        
        self.session.sendMessage(message, replyHandler: replyHandler, errorHandler: errorhandler)
    } */
    
    /*func session(session: WCSession, didReceiveFile file: WCSessionFile) {
        if let filepath = backend.processFile(file.fileURL) {
            
            if(file.metadata != nil) {
            self.processQr(file.metadata!, filepath: filepath)
            }
            
            //else {
                //self.showImage(filepath, title: "",subtitle: "DEFAULT")
            //}
        }
     
            notify_all()
        
        if(DEBUG){
            print("DID RECEIVE FILE")
        }
        
    } */
    
    
    /*private func notify_all() {
        
       // let notif = NSNotification(name: "QRIMAGE_UPDATED",object: self)
        let notif = NSNotification(name: "QRIMAGE_UPDATED", object: self, userInfo:nil)
        
        //notify the qrcode view of the change
        NSNotificationCenter.defaultCenter().postNotification(notif)
           } */


    /*private func processQr(metadata:[String:AnyObject],filepath:String) ->Void {
        var title:NSString?
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(filepath, forKey: "IMAGE_PATH")
        defaults.synchronize()
        print("THIS IS THE FILEPATH: \(filepath)")
        
        //if let metainfo = file.metadata {
            title = metadata["TITLE"] as!String?
            let subtitle = metadata["SUBTITLE"]
            var subTitleText:String
            if(subtitle != nil){
                subTitleText = subtitle! as! String
            }
            else {
                subTitleText = "Scan your qrcode"
            }
            defaults.setObject(subTitleText, forKey: "SUBTITLE")
            defaults.synchronize()
            
            
            if(title != nil) {
                defaults.setObject(title, forKey: "TITLE")
                defaults.synchronize()
                //self.showImage(filepath, title: title! as String,subtitle:subTitleText)
            }
                
            else{
                //self.showImage(filepath, title: "",subtitle:subTitleText)
                
                
            }//}
        

    
    
    
    
    }*/


    
    func session(session: WCSession, var didReceiveUserInfo userinfo: [String : AnyObject]) {
        print("DID RECEIEVE USER DATA");
        let backend = WatchBackend.getInstance()
            if(userinfo.count == 0) {
            backend.removeAll()
            return
        }
        
        
        
        
        
        let order = ReorderItem(dict:userinfo)
        
        var exists = false;
        var i = 0;
        
        while(!exists && i < backend.latestOrders.count) {
            
            if(order.productId == backend.latestOrders[i].productId) {
                exists = true;
            }
            i++
            
        }
        
        if(exists) {
            return
        }
        
        backend.latestOrders.insert(order, atIndex: 0)
        backend.addNewImage(order.imagePath)
        
        
            if(userinfo["MAX"] != nil) {
                let max:Int = userinfo["MAX"] as! Int
                let toRemove = backend.latestOrders.count - max
                if(toRemove > 0) {
                    backend.remove(toRemove)
                }
            }
        
        
        let done_not = NSNotification(name: "RECEIVED_ORDERS", object: self)
        
        let center = NSNotificationCenter.defaultCenter()
        center.postNotification(done_not)
        
        
        
        
    }
    
    //Called when a file is sent to the watch
   /* func session(session: WCSession, didReceiveFile file: WCSessionFile) {
        
        let backend = WatchBackend.getInstance()
        if let filepath = backend.processFile(file.fileURL) {
            
            if(file.metadata != nil) {
                backend.processQr(file.metadata!, filepath: filepath)
            }
        }
        
        backend.notify_all()
        
        if(DEBUG){
            print("DID RECEIVE FILE")
        }
        
        
        
        
    } */

    /*func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        let mess = message["MESSAGE"] as! String
        let backend = WatchBackend.getInstance();
        switch(mess) {
        case "REMOVE_ALL":   backend.removeAll();
         replyHandler(["BLAJ":"bla"]);
        backend.notify_all(); break;
        case "RELOAD": backend.notify_all()
        default:break;
        }
       
        
    }*/
    
        
        
        
    //func session(session: WCSession, didReceiveMessage message: [String : AnyObject]) {
      //          }
        
    
    //}
    

    
}
