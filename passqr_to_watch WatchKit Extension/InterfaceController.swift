
//
//  InterfaceController.swift
//  passqr_to_watch WatchKit Extension
//
//  Created by Tobias Ednersson on 07/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity
//class InterfaceController: WKInterfaceController,WCSessionDelegate{
class InterfaceController:WKInterfaceController {
    
    @IBOutlet var titleText: WKInterfaceLabel!
       
    @IBOutlet var subtitle: WKInterfaceLabel!
    @IBOutlet var qrimage: WKInterfaceImage!
    let backend = WatchBackend.init()
    let DEBUG = true
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        if(WCSession.isSupported()) {
            //let session = WCSession.defaultSession()
            //session.delegate = self
            //session.activateSession()
            
        }
        else {
            print("WatchConnectivity not supported!")
        }
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        self.updateQr()
        
        
        /*let defaults = NSUserDefaults.standardUserDefaults()
        var subtitle:String?
        
        subtitle = defaults.objectForKey("SUBTITLE") as! String?
        
        
        if(subtitle == nil) {
            subtitle = Optional("DEFAULT_SUBTITLE")
        }
        
        
        let path = defaults.objectForKey("IMAGE_PATH")
        if(path != nil)  {
            let title = defaults.objectForKey("TITLE") as! String?
            let path2 = path! as! String
            print("I WILL ACTIVATE:  \(path2)")
            if(title != nil) {
                self.showImage(path2, title: title!,subtitle:subtitle! as! String)
            }
            
            else{
                self.showImage(path2, title: "",subtitle:subtitle! as! String)
            }
        }
        
        */
    let center = NSNotificationCenter.defaultCenter()
    center.addObserver(self, selector: "qrImageChanged:", name: "QRIMAGE_UPDATED", object: nil)
    
    }

    
    
    private func updateQr() ->Void {
        let defaults = NSUserDefaults.standardUserDefaults()
        var subtitle:String?
        
        subtitle = defaults.objectForKey("SUBTITLE") as! String?
        
        
        if(subtitle == nil) {
            subtitle = Optional("DEFAULT_SUBTITLE")
        }
        
        
        let path = defaults.objectForKey("IMAGE_PATH")
        if(path != nil)  {
            let title = defaults.objectForKey("TITLE") as! String?
            let path2 = path! as! String
            print("I WILL ACTIVATE:  \(path2)")
            if(title != nil) {
                //self.showImage(path2, title: title!,subtitle:subtitle! as! String)
            }
                
            else{
                //self.showImage(path2, title: "",subtitle:subtitle! as! String)
            }
        
            let thisimage = UIImage(contentsOfFile: path2)
            
            let block = {
                
                if(title != nil) {
                self.titleText.setText(title)
                }
                self.qrimage.setImage(thisimage)
                
                if(subtitle != nil) {
                self.subtitle.setText(subtitle)
                }
                }
            dispatch_async(dispatch_get_main_queue(),block)
        }
        

        
        }
        

        
        
    
    
    
    
    
    func qrImageChanged(notification:NSNotification) ->Void {
        
        print("NOTIFICATION RECEIVED!")
        self.updateQr()
    }
    
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        NSNotificationCenter.defaultCenter().removeObserver(self)
        super.didDeactivate()
    }

   /* func session(session: WCSession, didReceiveFile file: WCSessionFile) {
       
        
        
        if let filepath = backend.processFile(file.fileURL) {
            var title:NSString?
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(filepath, forKey: "IMAGE_PATH")
            defaults.synchronize()
            if let metainfo = file.metadata {
                
                title = metainfo["TITLE"] as!String?
                
                let subtitle = metainfo["SUBTITLE"]
                var subTitleText:String
                if(subtitle != nil){
                    subTitleText = subtitle! as! String
                }
                else {
                    subTitleText = "Scan your qrcode"
                }
                
        
                defaults.setObject(subTitleText, forKey: "SUBTITLE")
                defaults.synchronize()
                
                
                if(title != nil) {
                    defaults.setObject(title, forKey: "TITLE")
                    defaults.synchronize()
                    self.showImage(filepath, title: title! as String,subtitle:subTitleText)
                    }
                
                else{
                   self.showImage(filepath, title: "",subtitle:subTitleText)
                
                
                }}
            
            else {
                self.showImage(filepath, title: "",subtitle: "DEFAULT")
            }
        }
        if(DEBUG){
            print("DID RECEIVE FILE")
        }
        
            }
    
     */
   // private func showImage(filepath:String,title:String,subtitle:String) ->Void {
        
    








  


}
