//
//  ReorderItem.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 26/11/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
// An implementation of the Reorder protocol
//This object is used in the watchapp to represent an item 
// which can be reordered

import UIKit

class ReorderItem: NSObject, ReorderProtocol {
    
    
    var productId:String
    var productName:String
    var imagePath:String
    var Price:Double
    
    
    required init(dict:[String:AnyObject]) {
        self.productId = dict["productId"] as! String;
        self.productName = dict["productName"]as! String
        self.imagePath = dict["imagePath"] as! String
        self.Price = dict["Price"] as! Double
        
    }
    
    func toDict() -> [String:AnyObject] {
        let toReturn = [
                        "productId":self.productId,
                        "productName" : self.productName,
                        "imagePath" : self.imagePath,
                        "Price":self.Price
            
        
        ]
    
        return toReturn as! [String:AnyObject]
    
    }
    

    init(productId:String,productName:String,imagePath:String,Price:Double) {
        self.productId = productId
        self.productName = productName
        self.imagePath = imagePath
        self.Price = Price
        super.init()
    }

}
