//
//  Shared.h
//  Shared
//
//  Created by Tobias Ednersson on 20/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Shared.
FOUNDATION_EXPORT double SharedVersionNumber;

//! Project version string for Shared.
FOUNDATION_EXPORT const unsigned char SharedVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Shared/PublicHeader.h>


