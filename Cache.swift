//
//  Cache.swift
//  passqr_to_watch
//  A simple image cache
//  Created by Tobias Ednersson on 08/12/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import WatchKit
import Foundation

class Cache: NSObject {

    //This hash is used to keep track of which images are being downloaded at the moment
    var imageBeingLoaded:[String:Bool] = [:]
    
    /*
        Takes an url as a string and converts it to a filename
        replacing all / with  _
    */
    private func urlToFileName(urlToConvert:String) ->String {
        let tempdir  = NSTemporaryDirectory()
        let result =  tempdir + urlToConvert.stringByReplacingOccurrencesOfString("/", withString: "_")
        return result
    }
    
    /*
        Given an UIImage and an url
        writes the image to a file with the same name as the url
    */
    private func writeToDisc(image:UIImage,url:String) {
        let savepath = urlToFileName(url)
        UIImageJPEGRepresentation(image, 100)?.writeToFile(savepath, atomically: true);
        
    }
    

    
    
    /*
        Given a url retrieves the image located at this url and
        runs the provided block with the image as argument
    
    */
    func getImage(url:String, todo:(image:UIImage) ->Void,index:Int) ->Void {
        //convert the url to the corresponding filename
        let filetofind = self.urlToFileName(url)
        let backend = WatchBackend.getInstance()
        //check if fileexists (the image has previously been downloaded)
        let manager = NSFileManager.defaultManager();
        
        if(manager.fileExistsAtPath(filetofind)) {
            //if this is the case then load the image from disc and run the provided block
            let image = UIImage(contentsOfFile: filetofind)
            todo(image: image!)
            
        }
        //  Image not downloaded
        else {

            //check if the image is beeing downloaded
            if(imageBeingLoaded[url] != nil) {
                //Download already in progress
                return
            }
            
            //why?
            if(!backend.isPaired()) {
                return
            }
            
            
            //Note that the image is being downloaded
            imageBeingLoaded[url] = true;
            
            //find the proper dimensions for the image
            let dimens = WKInterfaceDevice.currentDevice().screenBounds
            var measurement:Int
            if(dimens.height > dimens.width) {
                measurement = Int(floor(dimens.width))
            }
            
            else {
                measurement = Int(floor(dimens.width))
            }
        
            // the server will then scale the image properly
            let urlString = "http://images.silentorder.com/unsafe/\(measurement)x\(measurement )/smart/\(url)"
            let properUrl = NSURL(string: urlString)
            
            
            //Now download image
            let urlsession = NSURLSession.sharedSession()
            
            //this block will run when download has been completed
            let downloadHandler = { (location:NSURL?,response:NSURLResponse?,error:NSError?) in
                if(error != nil) {
                    print("\(error)")
                    return
                }
                //create the image
                let data = NSData(contentsOfURL: location!)
                let image = UIImage.init(data: data!)
                if(!(image == nil)) {
                    
                    //do whatever the caller wanted to do
                    todo(image: image!)
                    
                    //Then get the uithread and write the image to disc
                    let b = {() in
                        self.writeToDisc(image!, url: url)
                        
                    }
                    
                    dispatch_async(dispatch_get_main_queue(),b)
                    print("KONTAKT2")
                    self.writeToDisc(image!,url: url)
            }
        }
            
    if(properUrl != nil) {
        let downloadTask = urlsession.downloadTaskWithURL(properUrl!, completionHandler: downloadHandler)
        downloadTask.resume()
            }
        }
    }
}

