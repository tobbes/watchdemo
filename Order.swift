//
//  Order.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 25/11/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import Foundation
//

//  Order.swift

//  passqr_to_watch

//

//  Created by Tobias Ednersson on 16/10/15.

//  Copyright © 2015 Tobias Ednersson. All rights reserved.

// This class models an Order sent from the handheld to the watch */



import UIKit

@objc(Order)

class Order: NSObject{
    
    
    
    var productId:String =  ""
    
    var sortOrder:NSNumber = 0
    
    var Name:NSString = "A name"
    
    var Price:NSNumber = 77
    
    var imageId:NSString = ""
    
    var active:NSNumber = 0
    
    var type:NSString = "A"
    
    var tags:[String] = ["HEJ"]
    
    var prod_description:String =  "EN MYCKET FIN PRODUKT"
    
    var discountedPrice:NSNumber  = 77
    
    var discountedUnits:NSNumber = 5
    
    var offerId:NSString = "AAA"
    
    
    
    
    
    
    
    func toDict() -> [String:AnyObject] {
        
        
        
        let dict = [
            
            "productId":self.productId,
            
            "Name":self.Name,
            
            "sortOrder":self.sortOrder,
            
            "Price": self.Price,
            
            "active":self.active,
            
            "ImageId" : self.imageId,
            
            "type":self.type,
            
            "tags" : self.tags,
            
            "prod_description" : self.prod_description,
            
            "discountedUnits" : self.discountedUnits,
            
            "discountedPrice" : self.discountedPrice
            
        ]
        
        
        
        
        
        
        
        return dict
        
        
        
    }
    
    
    
    
    
    
    
    convenience init(dict:[String:AnyObject]) {
        
        
        
        
        
        self.init()
        
        self.productId = dict["productId"] as! String
        
        self.Name = dict["Name"] as! String
        
        self.sortOrder = dict["sortOrder"] as! NSNumber
        
        self.Price = dict["Price"] as! NSNumber
        
        self.active = dict["active"] as! NSNumber
        
        self.imageId = dict["ImageId"] as! String
        
        self.type = dict["type"] as! String
        
        self.tags = dict["tags"] as! [String]
        
        self.prod_description = dict["prod_description"] as! String
        
        self.discountedUnits = dict["discountedUnits"] as! NSNumber
        
        self.discountedPrice = dict["discountedPrice"] as! NSNumber
        
        
        
    }
    
    
    
    
    
    
    
    
    
}

















