//
//  MainRowType.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 21/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
// The rows of the table in ReorderActivity are all instances of this object

import WatchKit

class MainRowType: NSObject {
    @IBOutlet var group5: WKInterfaceGroup!

    @IBOutlet var mainBackgroundGroup: WKInterfaceGroup!

    @IBOutlet var buyButton: WKInterfaceButton!

    @IBOutlet var lowerGroup: WKInterfaceGroup!
    @IBOutlet var buttonImage: WKInterfaceImage!
    @IBOutlet var buttonImageGroup: WKInterfaceGroup!


    @IBOutlet var label: WKInterfaceLabel!



}