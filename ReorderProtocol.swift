//
//  ReorderProtocol.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 26/11/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//  This protocol must be implemented by any item to be 
//  synced to the watch
//

import Foundation
protocol ReorderProtocol {
    var productId:String {get}
    var productName:String {get}
    //the url of the productimage
    var imagePath:String {get}
    var Price:Double {get}
    
    
    init(dict:[String:AnyObject])
    
    //This function should transform the order into a dictionary, 
    // which can be transfered to the watch
    // the dictionary should have the following form:
    /* [
    "productId":
    "productName" :
    "imagePath" :
    "Price
    ] */
    func toDict() -> [String:AnyObject]
    
}