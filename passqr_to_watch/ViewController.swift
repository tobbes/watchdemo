//
//  ViewController.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 07/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import UIKit
import CoreImage
import WatchConnectivity

class ViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var titleText: UITextField!
    let backend = HandHeldBackend.init()
    
    @IBOutlet weak var subtitleText: UITextField!
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var textToConvert: UITextField!
    
        override func viewDidLoad() {
        super.viewDidLoad()
            
            
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func testMax1(sender: UIButton) {
    
        let item1 = ReorderItem(productId: "Vwxy5", productName: "Ett glas rosévin", imagePath:  "https://pinchosapi.silentorder.com/v1/images/pinchos/9-284-14473407605200100.png", Price: 70)
   
        
        backend.transferOrder(item1, max: 1)
    }
    
    
    
    
    @IBAction func max2(sender: AnyObject) {
    
        self.testMax2()
    }
    func testMax2() {
        
        var items:[ReorderProtocol]
        
        items = [ReorderItem(productId: "77dd", productName: "Pina Colada", imagePath: "https://pinchosapi.silentorder.com/v1/images/pinchos/1-554-14464707708430100.png", Price: 75),
                     ReorderItem(productId: "88ee", productName: "Riesling", imagePath: "http://images.silentorder.com/unsafe/300x300/smart/https://pinchosapi.silentorder.com/v1/images/pinchos/1-376-14310065474830200.png", Price: 100)
            
        ]
    
        backend.transferOrder(items,n: 2)
    
    }

    @IBAction func testMax3(sender: AnyObject) {
    

        let item1 = ReorderItem(productId: "C77AA", productName: "Ett glas Coca Cola", imagePath: "https://pinchosapi.silentorder.com/v1/images/pinchos/9-412-14371430052170200.png", Price: 20);
        let item2 = ReorderItem(productId: "D77AA", productName: "Ett glas fanta", imagePath:  "https://pinchosapi.silentorder.com/v1/images/pinchos/9-420-14413799285130200.png", Price: 20);
        let item3 = ReorderItem(productId: "E77AA", productName: "Ett glas äpplejuice", imagePath:"https://pinchosapi.silentorder.com/v1/images/pinchos/9-404-14474269631700100.png",
         Price: 10)
        
        let toSend:[ReorderProtocol] = [item1,item2,item3]
        backend.transferOrder(toSend, n: 3)
    
        
        
        
        
    }

    
    
    
    @IBAction func testOrderSync(sender: AnyObject) {
    
       
        
        let order = ReorderItem(productId: "AA77", productName: "En öl", imagePath: "https://pinchosapi.silentorder.com/v1/images/pinchos/1-31-14464665762000100.png", Price: 50)
                self.backend.transferOrder(order)
        
        let order2 = ReorderItem(productId: "zaa55", productName: "Brooklyn Brown ale", imagePath:"https://pinchosapi.silentorder.com/v1/images/pinchos/1-167-14464668175500100.png",  Price: 70)
        self.backend.transferOrder(order2)

    
    
    
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func SendNot(sender: AnyObject) {
        self.backend.TestLocalNotification(self.titleText.text,qrcontents: textToConvert.text,subtitle:self.subtitleText.text)
        self.view.resignFirstResponder()
    }
    
    
    
    
    
    @IBAction func testaTaBort(sender: AnyObject) {
        self.backend.removeAllOrders();
    
    }

       
    @IBAction func testSetList(sender: AnyObject) {
        
        
        
        
        let order1 = ReorderItem(productId: "z77", productName: "En flaska rödvin",
            imagePath: "https://pinchosapi.silentorder.com/v1/images/pinchos/1-589-14464670005530100.png"
 , Price: 100)
        
        
     let order2 = ReorderItem(productId: "Z88", productName: "Carlsberg Hof",
        imagePath: "https://pinchosapi.silentorder.com/v1/images/pinchos/1-30-14464667440770100.png", Price: 25)
        let order3 = ReorderItem(productId: "z99",productName: "White Ale",imagePath: "https://pinchosapi.silentorder.com/v1/images/pinchos/1-565-14489818286330100.png",Price: 50);

        var orders:[ReorderProtocol] = [];
        
        orders.append(order1);
        orders.append(order2)
        orders.append(order3)
        
        backend.setOrders(orders)
        
        
    
    
    }
    
    
    
    
    @IBAction func doGenerate(sender: UIButton) {
    
        if(textToConvert.text != nil) {
            
            
            var image:UIImage
            image = backend.generateQr(textToConvert.text!)
            
            
           
            self.imageview.image = image
            
            
            //let image2 = UIImage.init(data: data!)
            //self.imageview.image = image
            backend.doSendImage(image, title: "EN TITEL",subtitle:"BLAHA")
        }
    
        
        
    
    }


}

