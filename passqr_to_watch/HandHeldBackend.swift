//
//  HandHeldBackend.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 08/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import UIKit
import WatchConnectivity
import CoreImage



class HandHeldBackend: NSObject,WCSessionDelegate {
    let DEBUG = true
    
    override init() {
        super.init()
        if(WCSession.isSupported()) {
            
            let session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
            
        }
        
        else {
            print("WatchConnectivity not supported!")
        }
        
        
    }
    
    func session(session: WCSession, didFinishFileTransfer fileTransfer: WCSessionFileTransfer, error: NSError?) {
        print("INUTI DID FINISH")
        if(error != nil){
            print("Could not send file!")
            if(DEBUG) {
                print("Error: \(error?.localizedDescription)")
            }
        }
    
        else {
            if(DEBUG) {
                print("SUCCESSFULLY TRANSFERED FILE")
            }
        }

    }
    
    
    /*Sends a list of reorderitems to the  watch */
    func transferOrder(orders:[ReorderProtocol]) ->Void {
      let _  = orders.map(transferOrder)
        
        }
    
    

    /*  send a list of orders to watch assuring at most max orders on watch
    Hence, if the number of orders on watch orders.count are greater than max
    then max - orders.count orders will be removed from  the watch */


    func transferOrder(orders:[ReorderProtocol], n:Int) {
        
        if(orders.count > n) {
            //throw exception here
        }
        
        
        for o in orders {
            self.transferOrder(o,max: n)
        }
        
        
    }
    
    
    /*
    * Transfer the given reorderItem to the watchapp, assuring at most max items on watch
    */
    func transferOrder(order:ReorderProtocol,max:Int) {
        let session = WCSession.defaultSession()
        session.activateSession()
        var dict = order.toDict()
        dict["MAX"] = max
        session.transferUserInfo(dict)
        }
    
    
    /*Removes all orders currently on the watch and replaces them with the orders in the list 
    supplied */
    func setOrders(orders:[ReorderProtocol]) ->Void {
        let session = WCSession.defaultSession()
        session.delegate = self;
        session.activateSession()
        self.removeAllOrders()
        self.transferOrder(orders)
        }
    
    
    
    
    /*Sends a given reorderitem the watch */
    func transferOrder(o:ReorderProtocol) -> Void {
        let session = WCSession.defaultSession()
        let dict = o.toDict()
        session.transferUserInfo(dict)
    }
    
    /* Receives messages from the watch, specifically
        executes reordering of items, the implementation is left to the user of this library
    */
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
    
        let orderdict = message["ORDER"] as! [String:AnyObject]
        
        let order = ReorderItem(dict: orderdict)
      
        print(order.productName);
        
        
        let reply = ["REPLY" : "SUCESS"];
        replyHandler(reply)
        
    }
    
    
    /* Sends a qrcode as an UIImage to the watch 
        the provided title and subtitle are displayed over and under the qrcode respectively
        on the watch
    */
    func doSendImage(image:UIImage, title:String?,subtitle:String?) ->Void {
        let path = self.getFilePath()
        let imagedata = UIImagePNGRepresentation(image)
        imagedata!.writeToFile(path, atomically: true)
        let session = WCSession.defaultSession()
        session.delegate = self
        session.activateSession()
        let fileurl = NSURL.init(fileURLWithPath: path)
        var subtitleText: String
        if(subtitle != nil) {
            subtitleText = subtitle!
        }
        
        else {
            subtitleText = "Scanna din order"
        }
        
        if(title != nil) {
        session.transferFile(fileurl, metadata: ["TITLE":title!,"SUBTITLE":subtitleText,"ACTION":"QRIMAGE"])
        }
        else {
            session.transferFile(fileurl, metadata: ["TITLE":"Din orders qrkod","SUBTITLE":subtitleText])
        }
        
        
        if(DEBUG){
            print("DID TRY TO SEND FILE")
        }
        
}
    
    //Get the path to save the generated qr at
    private func getFilePath() ->String {
        let path = NSTemporaryDirectory()+"/image.png"
        if(DEBUG) {
            print("IMAGE SAVEPATH ON HANDHELD: \(path)")
        }
        
        return path
    
    }
    
    //Generates a qrcode of the provided string
    func generateQr(message:String) -> UIImage {
        let data = message.dataUsingEncoding(NSISOLatin1StringEncoding)
        let qrfilter = CIFilter(name: "CIQRCodeGenerator")
        qrfilter?.setValue(data, forKey: "inputMessage")
        qrfilter?.setValue("M", forKey: "inputCorrectionLevel")
        var qrcodeimage:CIImage
        qrcodeimage = (qrfilter?.outputImage)!
        
        let transform = CGAffineTransformMakeScale(5.0, 5.0);
        
        qrcodeimage = qrcodeimage.imageByApplyingTransform(transform)
        let context = CIContext.init(options: nil)
        let cgimage = context.createCGImage(qrcodeimage, fromRect: qrcodeimage.extent)
        let returnvalue = UIImage.init(CGImage: cgimage)
        return returnvalue
    }
 
    
    
    
    
    /* Removes all orderitems currently on the watch */
    func removeAllOrders() {
        let session = WCSession.defaultSession()
        session.delegate = self;
        session.activateSession()
        
        let removeCommand = [:]
        session.transferUserInfo(removeCommand as! [String : AnyObject])
    }
    
    
    /* This function just for testing sends a local notification to the watch */
    func TestLocalNotification(title:String?, qrcontents:String?,subtitle:String?) ->Void
    {
        let notification = UILocalNotification()
        notification.alertBody = "En jättefin body\n lorem ipsum...."
        notification.alertTitle = "EN MYCKET VIKTIG TITEL"
        let date = NSDate(timeIntervalSinceNow: 3)
        notification.fireDate = date
        
        var titleText:String
        
        if(title != nil) {
            
            titleText = title!
        }
        else {
            titleText = "EN FIN DEFAULTTITEL"
        }
        
        
        var qrText:String
        
        if(qrcontents != nil){
            qrText = qrcontents!
        }
        else {
            qrText = "DEFAULT INNEHÅLL"
        }
        
        
        var subtitleText:String
        
        
        if(subtitle != nil) {
            subtitleText = subtitle!
        }
        
        else {
            
            subtitleText = "STANDARD SUBTITLE"
            
        }
        
        let data = ["QRCODE":qrText,"TITLE":titleText,"SUBTITLE":subtitleText]
        
        notification.userInfo = data
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        print("Scheduled local notification")
    
    }

    
    
    


}
