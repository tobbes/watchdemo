//
//  Backend.swift
//  passqr_to_watch
//
//  Created by Tobias Ednersson on 07/10/15.
//  Copyright © 2015 Tobias Ednersson. All rights reserved.
//

import UIKit
import CoreImage
public class Backend: NSObject {

    
       public func generateQr(contents:String) -> UIImage {
        
        let data = contents.dataUsingEncoding(NSISOLatin1StringEncoding)
        let qrfilter = CIFilter(name: "CIQRCodeGenerator")
        
        qrfilter?.setValue(data, forKey: "inputMessage")
        qrfilter?.setValue("M", forKey: "inputCorrectionLevel")
        
        
        
        var qrcodeimage:CIImage
        qrcodeimage = (qrfilter?.outputImage)!
        
        let transform = CGAffineTransformMakeScale(5.0, 5.0);
        
        qrcodeimage = qrcodeimage.imageByApplyingTransform(transform)
        
        
        return UIImage.init(CIImage: qrcodeimage)
        
    }
    
    
}
